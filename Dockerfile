# This docker image is, as far as I know, the only way to reliably reproduce the
# issue. It uses `cargo chef` to make repeated use a little more bearable, but
# this should be completely transparent.
#
# To prepare the image:
#
#     docker build -t zmq-test-hack .
#
# To run the test:
#
#     docker run -it zmq-test-hack
#
# To run the test in a way that seems to reproduce the issue:
#
#     docker run -it --cpus=1 --cpuset-cpus=0 zmq-test-hack

FROM rust:1-slim AS base

ENV DEBIAN_FRONTEND=noninteractive
ENV PROJECT_DIR="/zmq-test"

RUN apt-get update && \
    apt-get -y -q install \
    g++ libzmq3-dev pkg-config procps strace

RUN cargo install cargo-chef

FROM base AS prep

ADD . "$PROJECT_DIR"
WORKDIR "$PROJECT_DIR"
RUN cargo chef prepare --recipe-path recipe.json

FROM base AS cook

WORKDIR "$PROJECT_DIR"
COPY --from=prep "$PROJECT_DIR/recipe.json" "$PROJECT_DIR/recipe.json"
RUN cargo chef cook --recipe-path recipe.json

FROM cook AS hack

ADD . "$PROJECT_DIR"
WORKDIR "$PROJECT_DIR"
ENV RUST_BACKTRACE=full
CMD ["cargo", "run", "pub"]
