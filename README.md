# ZMQ PUB-SUB missed message test

This project is the most minimal, most reliable way I could find to reproduce a message-dropping issue I've encountered with ZeroMQ missing messages in pub/sub patterns.

To use the project, run `cargo run pub`.

## Reproducing with the docker image

The docker image is, as far as I know, the only way to reliably reproduce the issue. It uses `cargo chef` to make repeated use a little more bearable, but this should be completely transparent.

To prepare the image:

    docker build -t zmq-test-hack .

To run the test:

    docker run -it zmq-test-hack

To run the test in a way that seems to reproduce the issue:

    docker run -it --cpus=1 --cpuset-cpus=0 zmq-test-hack

## Context

Let me start by saying: yes, I know that pub/sub sockets are specifically _not_ about completely reliable message delivery. Please hear me out for the context.

I encountered this issue when trying to write integration tests for a larger system, and because they were tests, I had these competing constraints of:

- I want to emulate a scenario that might occur in a real, long-running system
- I don't want my tests to take hours to run

So certain tests took the form of: start up two processes, one pub, one sub. Use socket events or a "side-channel" (like a pipe) to synchronise them on when the sockets are ready (eg. when the subscriber is connected and subscribed, when the publisher has bound). Now we can pretend we're testing pub/sub between established processes!

Except that sometimes, these particular tests would hang in CI. What was really happening under the hood was that some tests would fail to receive the first message in the test. After a lot of digging, I found I could reproduce the issue *only* by running under docker with settings that force it to run (and be pinned to) a single CPU.

The problem was `*cough cough*` solved by adding a 500-1500ms sleep after the sync point. I was not super happy with this, because I still didn't know why this was needed. I thought I understood how ZeroMQ and sockets worked under the hood, but this did not line up with my understanding.

Digging deeper into this only made me more confused. I went as far as to monitor socket events and only proceed on a `HANDSHAKE_SUCCEEDED` event. The issue remained. I went further and made the subscriber process signal _their_ `HANDSHAKE_SUCCEEDED` event to the publish process. The issue remained. I made sure that the pipe between process and subprocess was not causing the deadlock. It was not. I left the test deadlocked while monitoring any further socket events in other threads. Nothing.

So yes, while I fully understand that pub/sub systems are not meant to be reliable by themselves, and yes I have read the _Reliable PUB/SUB Patterns_ chapter and use them when needed... I really, really am curious about this and just want to know _why_ this happens. Why is it not enough to wait for `HANDSHAKE_SUCCEEDED` in both processes? What is happening during that extra 500ms that I can't simply check for in some other, more reliable way?

(You might ask: why not do what that chapter suggests and have a `DEALER`/`DEALER` pair to synchronise instead of messing around with `stdin`/`stdout` pipes and event monitors to do it? And the answer is, similarly, (a) I want to know more about what ZeroMQ is doing here, and (b) in my original case, I don't want to introduce extra ZeroMQ sockets into tests that are meant to be testing ZeroMQ integration.)

## The issue

- Start a process, call it "pub". "Pub"'s job is to send one single message on a `PUB` socket.
- This process spawns a subprocess, call it "sub". "Sub"'s job is to receive this message and immediately exit.
- "Sub" is spawned with `stdin` closed, `stdout` piped to "pub", and `stderr` inherited for debugging.
- "Sub" generates an IPC endpoint name. (In my original tests, these names are random. In this MRE, they are not.)
- "Sub" creates a `SUB` socket and connects to the endpoint.
- "Sub" prints the endpoint name to `stdout`.
- "Sub" starts monitoring socket events on the `SUB` socket. It will not proceed until it receives a `HANDSHAKE_SUCCEEDED` event.
- "Pub" reads this endpoint name.
- "Pub" creates a `PUB` socket and binds to the endpoint.
- "Pub" starts monitoring socket events on the `PUB` socket. It will not proceed until it receives a `HANDSHAKE_SUCCEEDED` event.
- "Pub" receives the `HANDSHAKE_SUCCEEDED` event from the monitor. It then waits for an empty line from "sub" on the subprocess' `stdout`.
- "Sub" receives the `HANDSHAKE_SUCCEEDED` event. It then prints an empty line to `stdout`.
- "Sub" immediately proceeds to a blocking receive call on its `SUB` socket.
- "Pub" receives the empty line from "sub"'s `stdout`. It immediately proceeds to a blocking send on its `PUB` socket.
- "Pub"'s call to `send_multipart()` returns.
- "Pub" does a blocking wait for "sub" to complete and reaps its PID.


Here is where the behaviour diverges. On most systems:

- "Sub" receives the message and exits.
- "Pub"'s blocking wait on the "sub" subprocess returns.
- "Pub" exits.

On systems with a single thread, single core (eg. embedded single-core CPU, docker using `--cpus=1 --cpuset-cpus=0` flags, most free-tier CI systems):

- "Sub" does not receive the message.
- "Sub"'s blocking receive never returns.
- "Pub"'s blocking wait on "sub" never returns.
- Both processes are deadlocked.

To summarise:

- I know that PUB sockets will drop messages if no subscribers are present
- I am specifically doing an elaborate dance to ensure the sockets are bound, connected, subscribed and ready before sending the first PUB message
- The first PUB message is nonetheless missed by the subscriber
- ...but only on systems with a single core, single thread (or simulated as such)
- I really want to know _why_ so I can understand ZeroMQ better

Workarounds:

- Use the documented "Reliable PUB/SUB Patterns" to synchronise the two processes
- Use timeouts or repeated sends to break the deadlock

Things that ruin the MRE ie. make it work under the single-CPU docker run:

- Don't wait for a randomly generated endpoint, just use a common one.
- Sleep in "pub" for about 500ms after the empty line is received from "sub".
- Sleep in "sub" for about 500ms after the handshake is detected but before sending the empty line.

All of these things, I think, effectively do the same thing: introduce an extra delay between "pub" seeing the handshake succeed, and sending the message.

Things that don't fix it:

- Tweaking socket buffer sizes via the `SNDBUF` setting in "pub" or `RCVBUF` in "sub".
- Tweaking the high water mark setting.
