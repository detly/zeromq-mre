use crate::{
    log::trace,
    socketry::{wait_for_handshake, watch_events},
};
use anyhow::Context;
use std::{
    io::{BufRead, BufReader},
    process::{Command, Stdio},
};

const TERMINATION_MSG: &[&[u8]] = &[b"\0", b"\0"];

pub(crate) fn run_as_pub() -> anyhow::Result<()> {
    trace!("start");

    trace!("launching subprocess");

    // Launch the subprocess we're sending to and wrap its output in a buffered
    // reader.
    let mut subproc = Command::new(std::env::current_exe()?)
        .arg(crate::MODE_SUB)
        .stdout(Stdio::piped())
        .stdin(Stdio::null())
        .stderr(Stdio::inherit())
        .spawn()?;

    trace!("subprocess PID = {}", subproc.id());

    let mut subproc_reader = BufReader::new(subproc.stdout.take().context("No stdout")?);

    // Read the "randomly generated" endpoint name.
    let mut endpoint = String::new();
    subproc_reader.read_line(&mut endpoint)?;
    endpoint = endpoint.trim().to_owned();

    trace!("endpoint = {endpoint:?}");

    let context = zmq::Context::new();
    let socket = context.socket(zmq::SocketType::PUB)?;

    // Set timeouts so our tests don't hang.
    socket.set_rcvtimeo(-1)?;
    socket.set_sndtimeo(-1)?;
    socket.set_connect_timeout(0)?;
    socket.bind(&endpoint).map_err(|error| {
        let context = if error == zmq::Error::EADDRINUSE {
            format!(
                "Socket endpoint {endpoint:?} is already in use. Use \"killall zmq-test\" or \
                find the offending process with \"ss -pax 'src @zmq-test-ipc'\".",
            )
        } else {
            format!("Error connecting to {endpoint:?}")
        };
        anyhow::anyhow!(error).context(context)
    })?;

    let monitor_endpoint = watch_events(&context, &socket)?;
    wait_for_handshake(&context, &monitor_endpoint)?;

    {
        trace!("waiting for empty line");

        let mut buf = String::new();
        subproc_reader.read_line(&mut buf)?;
        anyhow::ensure!(buf == "\n");

        trace!("got empty line");
    }

    trace!("sending termination message = {TERMINATION_MSG:?}",);

    socket.send_multipart(TERMINATION_MSG, 0)?;

    trace!("waiting for subprocess");

    let result = subproc.wait().context("Test responder did not run")?;
    anyhow::ensure!(
        result.success(),
        "Test responder produced a failure status {result:?}"
    );

    trace!("subprocess finished");

    trace!("end");

    Ok(())
}
