use crate::{
    log::trace,
    socketry::{wait_for_handshake, watch_events},
};

const ENDPOINT: &str = "ipc://@zmq-test-ipc";

pub(crate) fn run_as_sub() -> anyhow::Result<()> {
    trace!("start");

    let context = zmq::Context::new();
    let socket = context.socket(zmq::SocketType::SUB)?;

    socket.set_rcvtimeo(-1)?;
    socket.set_connect_timeout(0)?;
    socket.connect(ENDPOINT)?;

    trace!("created socket");

    socket.set_subscribe(b"\0").unwrap();

    // Print socket name to stdout for parent process to use.
    println!("{ENDPOINT}");

    let endpoint = watch_events(&context, &socket)?;
    wait_for_handshake(&context, &endpoint)?;

    // Signal we're ready.
    println!();

    loop {
        trace!("loop: receiving data on socket");
        let full = socket.recv_multipart(0).unwrap();

        trace!("loop: received data {full:?}");

        // Throw away the topic (it's the first frame of any message received on
        // a SUB socket).
        let message = full.split_first().unwrap().1;

        if message == vec![vec![0]] {
            trace!("loop: termination message received, breaking");
            break;
        }
    }

    trace!("end");
    Ok(())
}
