//! ZeroMQ utility stuff.

use crate::log::trace;

fn recv_full_message(socket: &zmq::Socket) -> zmq::Result<Vec<zmq::Message>> {
    let mut messages = vec![];

    loop {
        let message = socket.recv_msg(0)?;
        messages.push(message);
        if !socket.get_rcvmore()? {
            break;
        }
    }

    Ok(messages)
}

fn decode_event(msg: &[zmq::Message]) -> Option<zmq::SocketEvent> {
    if let [event, _endpoint] = msg {
        if let [event_n0, event_n1, _event_d0, _event_d1, _event_d2, _event_d3] = **event {
            let event_n = u16::from_ne_bytes([event_n0, event_n1]);
            let event = zmq::SocketEvent::from_raw(event_n);
            return Some(event);
        }
    }

    trace!("socket event did not have the expected structure or contents");

    None
}

fn watch_events_inner(
    context: &zmq::Context,
    endpoint_in: &str,
    endpoint_out: &str,
) -> anyhow::Result<()> {
    trace!("start");

    let monitor = context.socket(zmq::SocketType::PAIR)?;

    monitor.set_rcvtimeo(-1)?;
    monitor.set_connect_timeout(0)?;
    monitor.connect(endpoint_in)?;

    let forward = context.socket(zmq::SocketType::PAIR)?;

    forward.set_linger(-1)?;
    forward.bind(endpoint_out)?;

    loop {
        let msg = recv_full_message(&monitor);

        match msg {
            Ok(msg) => {
                if let Some(event) = decode_event(&msg) {
                    trace!("event = {event:?}");
                } else {
                    trace!("socket event did not have the expected structure or contents");
                }

                trace!("forwarding");
                forward.send_multipart(msg, 0)?;
            }

            Err(error) => {
                trace!("error on monitor socket: {error:#}");
            }
        }
    }
}

pub(crate) fn watch_events(context: &zmq::Context, socket: &zmq::Socket) -> anyhow::Result<String> {
    trace!("start");

    let endpoint = "inproc://@monitor";
    let endpoint_out = format!("{endpoint}-ex");

    socket.monitor(endpoint, zmq::SocketEvent::ALL.to_raw().into())?;

    trace!("spawning thread");

    std::thread::spawn({
        let context = context.clone();
        let endpoint_out = endpoint_out.clone();
        move || {
            trace!("thread started");
            watch_events_inner(&context, endpoint, &endpoint_out)
                .expect("Error in event watching thread");
        }
    });

    trace!("end");

    Ok(endpoint_out)
}

pub(crate) fn wait_for_handshake(
    context: &zmq::Context,
    monitor_endpoint: &str,
) -> anyhow::Result<()> {
    trace!("start");

    let monitor = context.socket(zmq::SocketType::PAIR)?;
    monitor.set_rcvtimeo(-1)?;
    monitor.set_connect_timeout(0)?;
    monitor.connect(monitor_endpoint)?;

    loop {
        let messages = recv_full_message(&monitor)?;

        if let Some(zmq::SocketEvent::HANDSHAKE_SUCCEEDED) = decode_event(&messages) {
            trace!("HANDSHAKE_SUCCEEDED event detected");
            break;
        }
    }

    trace!("end");

    Ok(())
}
