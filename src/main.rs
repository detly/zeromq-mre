//! This program's intended use is to run in "pub" mode, which will then spawn
//! the program in "sub" mode as a subprocess.

mod log;
mod publish;
mod socketry;
mod subscribe;

use crate::{log::init_log, publish::run_as_pub, subscribe::run_as_sub};
use anyhow::Result;
use log::trace;

const MODE_PUB: &str = "pub";
const MODE_SUB: &str = "sub";

fn main() -> Result<()> {
    let arg = std::env::args().nth(1);

    let (tag, thing_to_run): (_, fn() -> _) = match arg.as_deref() {
        Some(MODE_PUB) => (MODE_PUB, run_as_pub),
        Some(MODE_SUB) => (MODE_SUB, run_as_sub),
        _ => anyhow::bail!("Usage: zmq-test [pub|sub]"),
    };

    init_log(tag, module_path!());

    trace!("running in {tag:?} mode");

    thing_to_run()
}
