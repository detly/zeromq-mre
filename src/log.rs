//! Utilities for tracing and debugging this thing. Calling `trace!("format
//! string", ...)` from any function will prefix the format string with this
//! program's "tag" (ie. "pub" or "sub" mode) and the function it's being called
//! from.

use once_cell::sync::OnceCell;

static MODULE_ROOT: OnceCell<&'static str> = OnceCell::new();
static PROCESS_TAG: OnceCell<&'static str> = OnceCell::new();

/// From https://crates.io/crates/stdext
macro_rules! function_name {
    () => {{
        fn f() {}
        fn type_name_of<T>(_: T) -> &'static str {
            std::any::type_name::<T>()
        }
        let name = type_name_of(f);
        &name[..name.len() - 3]
    }};
}

macro_rules! trace {
    ($( $whatever:expr ),+ $(,)?) => {
        $crate::log::trace_log($crate::log::function_name!(), format_args!($( $whatever, )*))
    };
}

pub(crate) fn trace_log(func: &'static str, args: std::fmt::Arguments) {
    let tag = PROCESS_TAG.get().unwrap();
    let root = MODULE_ROOT.get().unwrap();
    let func = func
        .strip_prefix(root)
        .and_then(|txt| txt.strip_prefix("::"))
        .unwrap_or(func);
    eprintln!("[{tag}] {func}(): {args}");
}

pub(crate) fn init_log(tag: &'static str, root: &'static str) {
    PROCESS_TAG.set(tag).unwrap();
    MODULE_ROOT.set(root).unwrap();
}

pub(crate) use {function_name, trace};
